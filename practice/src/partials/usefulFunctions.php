<?php

use JetBrains\PhpStorm\ArrayShape;

function importData(string $fileName): array
{
	$rawData = file_get_contents("../../input/$fileName");
	$lines = explode("\n", $rawData);

	$output = [];
	foreach ($lines as $row => $line) {
		if ($line === '') {
			continue;
		}

		$lineData = explode(' ', $line);
		if ($row === 0) {
			$output = [
				'maxPizzas' => (int)$lineData[0],
				'2TeamCount' => (int)$lineData[1],
				'3TeamCount' => (int)$lineData[2],
				'4TeamCount' => (int)$lineData[3],
				'totalTeamCount' => $lineData[1] + $lineData[2] + $lineData[3],
				'pizzaTypes' => [],
			];
		} else {
			foreach ($lineData as $col => $datum) {
				if ($col === 0) {
					$output['pizzaTypes'][$row - 1] = [
						'ingredientCount' => (int)$datum,
						'ingredients' => []
					];
				} else {
					$output['pizzaTypes'][$row - 1]['ingredients'][] = $datum;
				}
			}
		}
	}

	return $output;
}

function exportData(string $fileName, array $deliveries)
{
	$filePath = "../../output/$fileName";

	// Line 1 is count
	file_put_contents($filePath, count($deliveries) . "\n");

	// Other lines contain team size and pizzas delivered
	foreach ($deliveries as $delivery) {
		file_put_contents($filePath, $delivery['teamSize'], FILE_APPEND);
		foreach ($delivery['pizzas'] as $pizza) {
			file_put_contents($filePath, " $pizza", FILE_APPEND);
		}
		file_put_contents($filePath, "\n", FILE_APPEND);
	}

	echo "Data written to file: '$filePath'";
}

#[ArrayShape(['teamSize' => "int", 'pizzas' => "array"])]
function createDelivery(int $teamSize, array $pizzas): array
{
	return ['teamSize' => $teamSize, 'pizzas' => $pizzas];
}