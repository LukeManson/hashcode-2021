<?php

include '../partials/usefulFunctions.php';

$problemName = 'e';
$data = importData($problemName);

$deliveries = [];

for ($i = 0; $i < $data['4TeamCount']; $i++) {
	$teamSize = 4;
	$pizzas = [];
	$ingredients = [];

	foreach ($data['pizzaTypes'] as $pizzaIdx => $pizzaType) {
		$pizzas[] = $pizzaIdx;
		$ingredients = array_merge($ingredients, $pizzaType['ingredients']);

		if (count($pizzas) === $teamSize) {
			foreach ($pizzas as $p) {
				unset($data['pizzaTypes'][$p]);
			}
			$deliveries[] = createDelivery(4, $pizzas);
			break;
		}
	}

}

exportData($problemName.'/'.basename(__FILE__, '.php'), $deliveries);