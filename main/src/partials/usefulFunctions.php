<?php

use JetBrains\PhpStorm\ArrayShape;

function importData(string $fileName): array
{
	$rawData = file_get_contents("../../input/$fileName");
	$lines = explode("\n", $rawData);

	$output = [];
	$streetsTemp = [];
	foreach ($lines as $row => $line) {
		if ($line === '') {
			continue;
		}

		$lineData = explode(' ', $line);
		if ($row === 0) {
			$output = [
				'simulationDuration' => (int)$lineData[0],
				'intersectionCount' => (int)$lineData[1],
				'streetCount' => (int)$lineData[2],
				'carCount' => (int)$lineData[3],
				'pointsPerJourney' => $lineData[1] + $lineData[2] + $lineData[3],
				'streets' => [],
				'cars' => [],
			];
		} else {
			foreach ($lineData as $col => $datum) {
				if ($row < $output['streetCount'] + 1) {
					static $streetColMap = [
						'startIntersection',
						'endIntersection',
						'name',
						'travelTime',
						'pointsForJourney'
					];

					static $castStreetMap = [
						'int',
						'int',
						'str',
						'int',
						'int'
					];

					$streetsTemp[$row - 1][$streetColMap[$col]] = $castStreetMap[$col] === 'int' ? (int)$datum : $datum;
				}
				else {
					if ($col === 0) {
						$output['cars'][$row - $output['streetCount']]['streetTravelCount'] = (int)$datum;
					} else {
						$output['cars'][$row - $output['streetCount']]['streetNames'][] = $datum;
					}
				}
			}
		}
	}

	foreach ($streetsTemp as $street) {
		$output['streets'][$street['name']] = $street;
	}

	// Calculate total travel time
	foreach ($output['cars'] as $idx => $car) {
		$travelTime = 0;
		foreach ($car['streetNames'] as $streetName) {
			$travelTime += $output['streets'][$streetName]['travelTime'];
		}
		$output['cars'][$idx]['totalTravelTime'] = $travelTime;
	}

	return $output;
}

function exportData(string $fileName, array $schedules)
{
	$filePath = "../../output/$fileName";

	// Line 1 is count
	file_put_contents($filePath, count($schedules) . "\n");

	// Other lines contain team size and pizzas delivered
	foreach ($schedules as $schedule) {
		file_put_contents($filePath, $schedule['id'] . "\n", FILE_APPEND);
		file_put_contents($filePath, count($schedule['streets']) . "\n", FILE_APPEND);
		foreach ($schedule['streets'] as $name => $duration) {
			file_put_contents($filePath, $name . ' ' . $duration . "\n", FILE_APPEND);
		}
	}

	echo "Data written to file: '$filePath'\n";
}

#[ArrayShape(['id' => "int", 'streets' => "array"])]
function createIntersectionSchedule(int $intersectionId, array $streets): array
{
	return ['id' => $intersectionId, 'streets' => $streets];
}


