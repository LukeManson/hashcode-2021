<?php

include '../partials/usefulFunctions.php';

foreach (['a', 'b', 'c' ,'d' ,'e', 'f'] as $problemName) {
	$data = importData($problemName);

	// start all at 1
	$intersections = [];
	foreach ($data['streets'] as $street) {
		$intersections[$street['endIntersection']]['streets'][$street['name']] = 1;
	}

	// get start count at each intersection
	foreach ($data['cars'] as $cars) {
		$intersectionId = $data['streets'][$cars['streetNames'][0]]['endIntersection'];
		$intersections[$intersectionId]['streets'][$cars['streetNames'][0]]++;
	}

	$schedule = [];
	foreach ($intersections as $idx => $intersection) {
		$schedule[] = createIntersectionSchedule($idx, $intersection['streets']);
	}

	exportData($problemName . '/' . basename(__FILE__, '.php'), $schedule);
}
