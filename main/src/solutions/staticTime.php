<?php

include '../partials/usefulFunctions.php';

$problemName = 'f';
$data = importData($problemName);

for ($time = 2; $time < 6; $time ++) {
	$intersections = [];
	foreach ($data['streets'] as $street) {
		$intersections[$street['endIntersection']]['streets'][$street['name']] = $time;
	}

	$schedule = [];
	foreach ($intersections as $idx => $intersection) {
		$schedule[] = createIntersectionSchedule($idx, $intersection['streets']);
	}

	exportData($problemName.'/'.basename(__FILE__, '.php') . '_' . $time, $schedule);
}
