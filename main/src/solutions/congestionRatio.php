<?php

include '../partials/usefulFunctions.php';

foreach (['a', 'b', 'c' ,'d' ,'e', 'f'] as $problemName) {
	$data = importData($problemName);

	$simulationDuration = $data['simulationDuration'];
	$cars = $data['cars'];
	$streets = $data['streets'];
	$intersections = [];

	foreach ($cars as $car) {
		foreach ($car['streetNames'] as $streetName) {
			if (!isset($streets[$streetName]['totalCars'])) {
				$streets[$streetName]['totalCars'] = 1;
			} else {
				$streets[$streetName]['totalCars'] ++;
			}

//			$intersectionId = $streets[$streetName]['endIntersection'];
//			if (!isset($intersections[$intersectionId]['totalCars'])) {
//				$intersections[$intersectionId]['totalCars'] = 1;
//			} else {
//				$intersections[$intersectionId]['totalCars']++;
//			}
		}
	}

	foreach ($streets as $street) {
		$intersections[$street['endIntersection']]['streetsEnd'][] = $street['name'];
	}

	$schedules = [];
	foreach ($intersections as $intersectionId => $intersection) {
		$leastCars = null;
		foreach ($intersection['streetsEnd'] as $streetName) {
			if (!isset($streets[$streetName]['totalCars'])) {
				continue;
			}
			if ($leastCars === null || $leastCars > $streets[$streetName]['totalCars']) {
				$leastCars = $streets[$streetName]['totalCars'];
			}
		}
		foreach ($intersection['streetsEnd'] as $streetName) {
			if (!isset($streets[$streetName]['totalCars'])) {
				continue;
			}
			$schedules[$intersectionId]['streets'][$streetName] = ceil(($streets[$streetName]['totalCars']/($leastCars/3.1415926)));
		}
	}

	$finalSchedule = [];
	foreach ($schedules as $idx => $schedule) {
		$finalSchedule[] = createIntersectionSchedule($idx, $schedule['streets']);
	}

	exportData($problemName . '/' . basename(__FILE__, '.php') . '_divPi', $finalSchedule);
}
